public class lambdatest1{
    public static void main (String [] args) {

      //expresion lambda ==> representa un objeto de una interfaz funcional 
      FunctionTest1 ft = ( ) -> System.out.println("Hola Mundo!!!"); //Implementacion del metodo abstracto saludar
      															//de la interfaz funcional 
      
      //ft.saludar();

      lambdatest1 objeto = new lambdatest1();
      objeto.miMetodo(ft);
     }

     public void miMetodo(FunctionTest1 parametro){
     	parametro.saludar();
     }
}