public class lambdatest2{
    public static void main (String [] args) {

      //expresion lambda ==> representa un objeto de una interfaz funcional 
      FunctionTest1 ft = ( ) -> System.out.println("Hola Mundo"); //Implementacion del metodo abstracto saludar
      															//de la interfaz funcional 
      
      ft.saludar();
     }
}