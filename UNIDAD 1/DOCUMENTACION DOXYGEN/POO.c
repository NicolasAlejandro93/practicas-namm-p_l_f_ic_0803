/**
@brief Estos son los archivos de cabecera que contienen las definiciones de las
macros, constantes, las declaraciones de funciones de la biblioteca
estandar del lenguaje C.*/
#include <stdio.h> 
#include <stdlib.h>

typedef struct
{

	int esta; /** valores enteros */
	int peso;  /** valores enteros */
	char sexo; /** valores en caracter */
} estudiante; /** clase estudiante */

/**
 * @brief      Esta función imprimir sirve para mostrar en pantalla...
 *
 * @param      e Este parametro nos sirve
 * @return     void
 */
void imprimir(estudiante *e);

/**
 * @brief      { function_description }
 *
 * @return     { description_of_the_return_value }
 */
int main ()

{
	estudiante *e = (estudiante *) malloc (sizeof (estudiante));

	printf("Escribe tu Estatura: ");
	scanf("%d", &e->esta);
	printf("ESTATURA: %d\n", e->esta);
	fflush(stdin);

	printf("Escribe tu Peso: ");
	scanf("%d", &e->peso);
	printf("PESO: %d\n", e->peso);
	fflush(stdin);

	printf("Escribe tu Sexo: ");
	printf("SEXO: %d\n", e->sexo);
	scanf("%c", &e->sexo);

	imprimir(e);
	fflush(stdin);
	return 0;
}

void imprimir(estudiante *e) 
{
	
	printf("ESTATURA: %d\n", e->esta);
	printf("PESO: %d\n", e->peso);
	printf("SEXO: %c\n", e->sexo);	
}