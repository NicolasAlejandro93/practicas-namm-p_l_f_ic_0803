public class LambdaTest22{
	public static void main(String [] args) {

	    //Expresion lambda ==> representa un objeto de una interfaz funcional 
		Operaciones2 op = (num1, num2) -> System.out.println(num1 + num2);

	    //op.imprimeOperacion(5, 10);

	    LambdaTest22 objeto = new LambdaTest22();
	    objeto.miMetodo (op, 10,10);
	  }

	    public void miMetodo(Operaciones2 op, int num1, int num2) {
	    	op.imprimeSuma(num1, num2);
	    }
	}