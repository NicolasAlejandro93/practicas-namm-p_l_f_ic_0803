public class LambdaTest23{
	public static void main(String [] args) {

	    //Expresion lambda ==> representa un objeto de una interfaz funcional 
		Operaciones21 op = (num1, num2) -> System.out.println(num1 + num2);

	    //op.imprimeOperacion(5, 10);

	    LambdaTest23 objeto = new LambdaTest23();
	    objeto.miMetodo ((num1, num2) -> System.out.println(num1 - num2), 20, 10);
	    
	    objeto.miMetodo((num1, num2) -> System.out.println(num1 * num2), 20, 10);

	    }

	    public void miMetodo(Operaciones21 op, int num1, int num2) {
	    	op.imprimeOperacion(num1, num2);
	    }
	}