public class LambdaTest2{
	public static void main(String[] args) {

	  //Expresion lambda ==> representa un objeto de una interfaz funcional 
	  Operaciones2 op = (num1, num2) -> System.out.println(num1 + num2);

	  op.imprimeSuma(5, 10); 
	}
}