#include <stdio.h>
#include <stdlib.h>

#include <malloc.h>

void insertarEnLista (tpuntero *cabeza, int e)
{
	tpuntero nuevo; //Creamos un nuevo nodo
	nuevo = malloc(sizeof(tnodo)); //Utilizamos malloc para reservar memoria para ese nodo
	nuevo->valor = e; //Le asignamos el valor ingresado por pantalla a ese nodo
	nuevo->sig = *cabeza; //Le asignamos al siguiente el valor de cabeza
	*cabeza = nuevo; //Cabeza pasa a ser el ultimo nodo agregado
}
 
void imprimirLista(tpuntero cabeza)
{
	while(cabeza != NULL)//Mientras cabeza no sea NULL
	{
		printf("%4d",cabeza->valor); //Imprimimos el valor del nodo
		cabeza = cabeza->sig; //Pasamos al siguiente nodo
	}
}
 
void borrarLista(tpuntero *cabeza)
{
	tpuntero actual; //Puntero auxiliar para eliminar correctamente la lista
	
	while(*cabeza != NULL)//Mientras cabeza no sea NULL
	{
		actual = *cabeza; //Actual toma el valor de cabeza
		*cabeza = (*cabeza)->sig; //Cabeza avanza 1 posicion en la lista
		free(actual); //Se libera la memoria de la posicion de Actual (el primer nodo), y cabeza queda apuntando al que ahora es el primero
	}
}
