#include <stdio.h>
#include <stdlib.h>

#include "lib_listas.h"
 
typedef struct snodo //snodo es el nombre de la estructura
{
	int valor;
	struct snodo *sig; //El puntero siguiente para recorrer la lista enlazada
}tnodo; //tnodo es el tipo de dato para declarar la estructura
 
typedef tnodo *tpuntero; //Puntero al tipo de dato tnodo para no utilizar punteros de punteros
 
//void insertarEnLista (tpuntero *cabeza, int e);
//void imprimirLista (tpuntero cabeza);
//void borrarLista (tpuntero *cabeza);
 
int main()
{
	int e;
	tpuntero cabeza; //Indica la cabeza de la lista enlazada, si la perdemos no podremos acceder a la lista
	cabeza = NULL; //Se inicializa la cabeza como NULL ya que no hay ningun nodo cargado en la lista
	printf("Ingrese elementos, -1 para terminar: ");
	scanf("%d",&e);
	
	while(e!=-1)
	{
		insertarEnLista (&cabeza, e);
		printf ("Ingresado correctamente");
		printf ("\n");
		printf("Ingrese elementos, -1 para terminar: ");
		scanf("%d",&e);
	}
	
	printf ("\nSe imprime la lista cargada: ");
	imprimirLista (cabeza);
	
	printf ("\nSe borra la lista cargada\n");
	borrarLista (&cabeza);
	
	printf ("\n");
	system ("PAUSE");
	
	return 0;
}
