import java.util.List;
	public class FP_Functional_Exercises{
	public static void main(String[] args) {
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		
		List<String> courses = List.of("Spring", "Spring Boot", "API",
		"Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");

		//ejercicio 1
		printEvenNumbersInListFunctional(numbers);
		//ejercicio 2
		printAllCourses(courses);
		//ejercicio 3
		printOnlineCourses(courses);
	 	//ejercicio 4
		printOnFourCourses(courses);
		//ejercicio 5
		printSquaresOfEvenNumbersInListFunctional(numbers);
		//ejercicio 6
		printNumberCharactersOfCoursesInListFunctional(courses);
	}	
	/*metodo para imprimir numeros*/
	private static void print(int numbers){
		System.out.println(numbers + "\n ");
	}
	/*metodo para ejercicio 1*/
	private static boolean isEven(int numbers){
		return (numbers % 2 != 0);	
	}
	/*metodo para imprimir la lista courses*/
	private static void printS(String courses){
		System.out.println(courses +"\n ");	
	}	
	/*metodo para ejercicio 5*/
	private static boolean isPar(int numbers){
	        return(numbers % 2 == 0 );
	}
	// ejercicio 1	
	private static void printEvenNumbersInListFunctional(List<Integer> numbers){
		System.out.println("Ejercicio 1: Odd Numbers");		
			numbers.stream()                  
				.filter(FP_Functional_Exercises::isEven)
				.forEach(FP_Functional_Exercises::print);
	}
	// ejercicio 2
	private static void printAllCourses(List<String> courses){
		System.out.println("Ejercicio 2: All Courses");
			courses.stream()
				.forEach(FP_Functional_Exercises::printS);
	}
	// ejercicio 3
	private static void printOnlineCourses(List<String> courses){
		System.out.println("Ejercicio 3: Only Word Spring");
			courses.stream()
				.filter(course -> course.contains ("Spring"))
				.forEach(FP_Functional_Exercises::printS);
	}
	// ejercicio 4
	private static void printOnFourCourses(List<String>courses){
		System.out.println("Ejercicio 4: Whose name has atleast 4 letters");
        		courses.stream()
			        .filter(course -> course.length()>= 4)
			        .forEach (FP_Functional_Exercises::printS);
	}
	// ejercicio 5
	private static void printSquaresOfEvenNumbersInListFunctional(List<Integer> numbers){
		System.out.println("Ejercicio 5: cubes of odd numbers");
    			numbers.stream()                        
        			.filter(FP_Functional_Exercises::isPar)
			        .map(number -> number * number)   
			        .forEach(FP_Functional_Exercises::print);
	}
	// ejercicio 6
	 private static void printNumberCharactersOfCoursesInListFunctional(List<String> courses){
		System.out.println("Ejercicio 6: characters in each course name");
        		courses.stream()
		            .map(course -> course.length())                         
		            .forEach(FP_Functional_Exercises::print);   
    }
}
