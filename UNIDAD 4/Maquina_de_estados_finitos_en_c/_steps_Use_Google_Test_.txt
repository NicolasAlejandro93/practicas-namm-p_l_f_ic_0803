fher@fher-PC ~/_REPO_fher_/_example_google_test $ sudo apt-cache search gtest
lxc-tests - Linux Containers userspace tools (test binaries)
debian-policy - Debian Policy Manual and related documents
gnome-desktop-testing - runner for GNOME installed tests
gtester2xunit - Helper for converting gtester xml output to xunit.
libdist-zilla-plugin-podspellingtests-perl - Backward-compatibility wrapper around Dist::Zilla::Plugin::Test::PodSpelling
libdist-zilla-plugin-test-podspelling-perl - Author tests for POD spelling
libgtest-dev - Google's framework for writing C++ tests - header files
libxorg-gtest-data - X.Org dummy testing environment for Google Test - data
libxorg-gtest-dev - X.Org dummy testing environment for Google Test - headers
libxorg-gtest-doc - X.org dummy testing environment for Google Test - documentation
pkg-perl-autopkgtest - collection of autopktest scripts for Perl packages
python-nose-timer - timer plugin for nosetests - Python 2.x
python3-nose-timer - timer plugin for nosetests - Python 3.x
autopkgtest - automatic as-installed testing for Debian packages
fher@fher-PC ~/_REPO_fher_/_example_google_test $ 

sudo apt-get install libgtest-dev
sudo apt-get install cmake
cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make
sudo cp *.a /usr/lib


touch sqrt.cpp
touch sqrt_test.cpp
touch CMakeLists.txt

-------------------------------------------------
sqrt.cpp
-------------------------------------------------
#include <math.h>

// Get the Square root of a number.
double squareRoot(const double a){
	double b = sqrt(a);
    	if(b != b){ // NaN check
    		return -1.0;
	}
    	else{
    		return sqrt(a);
    	}
}
-------------------------------------------------

-------------------------------------------------
sqrt_test.cpp
-------------------------------------------------
#include "sqrt.cpp"
#include <gtest/gtest.h>

TEST(SquareRootTest, PositiveNos){
    ASSERT_EQ(6, squareRoot(36.0));
    ASSERT_EQ(18.0, squareRoot(324.0));
    ASSERT_EQ(25.4, squareRoot(645.16));
    ASSERT_EQ(0, squareRoot(0.0));
}

TEST(SquareRootTest, NegativeNos){
    ASSERT_EQ(-1.0, squareRoot(-15.0));
    ASSERT_EQ(-1.0, squareRoot(-0.2));
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
-------------------------------------------------  

-------------------------------------------------
CMakeLists.txt
-------------------------------------------------
cmake_minimum_required(VERSION 2.6)
 
# Locate GTest
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})
 
# Link runTests with what we want to test and the GTest and pthread library
add_executable(executeTests sqrt_test.cpp)
target_link_libraries(executeTests ${GTEST_LIBRARIES} pthread)
-------------------------------------------------

cmake CMakeLists.txt
make
./executeTests